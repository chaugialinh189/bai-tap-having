﻿CREATE TABLE CustomerOrder(
		Customer_ID int primary key identity(1,1),
		FullName varchar(250) not null,
		DeliveryCity varchar(250) not null,
		OrderStatus varchar(30) not null,
		TotalAmount Decimal(10,2) not null check (TotalAmount >= 0),
		OrderDate date not null,
		DateDelivery Date not null
);
